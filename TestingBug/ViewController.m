//
//  ViewController.m
//  TestingBug
//
//  Created by Sanchyan Chakraborty on 08/08/21.
//

#import "ViewController.h"
@import PSPDFKit;
@import PSPDFKitUI;
PSPDFDocument *document;
NSArray *annotations;
UINavigationController *navController ;

@interface ViewController ()

@end

@implementation ViewController
- (IBAction)button1:(UIButton *)sender {
    
    NSURL *documentURL = [NSBundle.mainBundle URLForResource:@"test" withExtension:@"pdf"];
  
  document = [[PSPDFDocument alloc] initWithURL:documentURL];
    [document updateRenderOptionsForType:PSPDFRenderTypeAll withBlock:^(PSPDFRenderOptions * _Nonnull options) {
        options.interactiveFormFillColor = UIColor.clearColor;
    }];
    
    annotations = [document annotationsForPageAtIndex:0 type:PSPDFAnnotationTypeWidget];
    for (PSPDFFormElement *formElement in annotations) {
        if ([formElement isKindOfClass:PSPDFTextFieldFormElement.class]) {
            if([formElement.fieldName  isEqual: @"data_1"])
            {
                formElement.formField.value = @"PDF 1 Data";
                
            }
        }
    }
    document.title = @"PDF 1 Title";
    PSPDFViewController *pdfController = [[PSPDFViewController alloc] initWithDocument:document configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder *builder) {
        builder.createAnnotationMenuEnabled = NO;
        builder.editableAnnotationTypes = nil;
        builder.showBackActionButton = NO;
        builder.thumbnailBarMode = PSPDFThumbnailBarModeScrollable;
        builder.pageLabelEnabled = NO;
        
        
    }]];
    navController = [[UINavigationController alloc] initWithRootViewController:pdfController];
    navController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:navController animated:NO completion:NULL];
}
- (IBAction)button2:(id)sender {
    NSURL *documentURL = [NSBundle.mainBundle URLForResource:@"test" withExtension:@"pdf"];
  
  document = [[PSPDFDocument alloc] initWithURL:documentURL];
    [document updateRenderOptionsForType:PSPDFRenderTypeAll withBlock:^(PSPDFRenderOptions * _Nonnull options) {
        options.interactiveFormFillColor = UIColor.clearColor;
    }];
    
    annotations = [document annotationsForPageAtIndex:0 type:PSPDFAnnotationTypeWidget];
    for (PSPDFFormElement *formElement in annotations) {
        if ([formElement isKindOfClass:PSPDFTextFieldFormElement.class]) {
            if([formElement.fieldName  isEqual: @"data_2"])
            {
                formElement.formField.value = @"PDF 2 Data";
                
            }
        }
    }
    document.title = @"PDF 2 Title";
    PSPDFViewController *pdfController = [[PSPDFViewController alloc] initWithDocument:document configuration:[PSPDFConfiguration configurationWithBuilder:^(PSPDFConfigurationBuilder *builder) {
        builder.createAnnotationMenuEnabled = NO;
        builder.editableAnnotationTypes = nil;
        builder.showBackActionButton = NO;
        builder.thumbnailBarMode = PSPDFThumbnailBarModeScrollable;
        builder.pageLabelEnabled = NO;
        
        
    }]];
    navController = [[UINavigationController alloc] initWithRootViewController:pdfController];
    navController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:navController animated:NO completion:NULL];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"ViewController.m ViewDidLoad");
}


@end
