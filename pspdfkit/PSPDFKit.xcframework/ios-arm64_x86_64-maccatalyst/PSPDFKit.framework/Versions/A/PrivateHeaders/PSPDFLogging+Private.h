//
//  Copyright © 2011-2021 PSPDFKit GmbH. All rights reserved.
//
//  THIS SOURCE CODE AND ANY ACCOMPANYING DOCUMENTATION ARE PROTECTED BY INTERNATIONAL COPYRIGHT LAW
//  AND MAY NOT BE RESOLD OR REDISTRIBUTED. USAGE IS BOUND TO THE PSPDFKIT LICENSE AGREEMENT.
//  UNAUTHORIZED REPRODUCTION OR DISTRIBUTION IS SUBJECT TO CIVIL AND CRIMINAL PENALTIES.
//  This notice may not be removed from this file.
//

#if !defined(PSPDF_STATIC_BUILD)
#import <PSPDFKit/PSPDFLogging.h>
#import <PSPDFKit/PSPDFMacros+Private.h>
#import <PSPDFKit/PSPDFError+Private.h>
#import <PSPDFKit/PSPDFAssert.h>
#else
#import "PSPDFLogging.h"
#import "PSPDFMacros+Private.h"
#import "Error/PSPDFError+Private.h"
#import "PSPDFAssert.h"
#endif

NS_ASSUME_NONNULL_BEGIN

typedef void (^PSPDFLogHandler)(PSPDFLogLevelMask level, const char *_Nullable tag, NS_NOESCAPE NSString * (^message)(void), const char *_Nullable file, const char *_Nullable function, NSUInteger line);

/// Internal global variable for the log level. Set via PSPDFKitGlobal.sharedInstance.logLevel.
PSPDF_EXTERN PSPDFLogLevelMask PSPDFLogLevel;

/// Default handler for logging.
PSPDF_APP_EXPORT PSPDFLogHandler PSPDFLogHandlerBlock;

/// Internal logging function to make setting breakpoints easier.
PSPDF_EXTERN void _PSPDFLog(PSPDFLogLevelMask level, const char *_Nullable tag, NSString *lineString, NSString * (^message)(void));

NS_ASSUME_NONNULL_END

// Logging

#if defined(PSPDF_BLOCK_LOGGING)
#define PSPDF_LOG_USE_ONLY __unused
#else
#define PSPDF_LOG_USE_ONLY
#endif

#if defined(PSPDF_BLOCK_LOGGING)
#define PSPDFLogMacro(_level, _tag, _message)
#else
#define PSPDFLogMacro(_level, _tag, _message) PSPDFLogHandlerBlock(_level, _tag, _message, __FILE_NAME__, __PRETTY_FUNCTION__, __LINE__)
#endif

// Classical log macros without tag. These are for legacy use.
// Prefer tagged versions (TLog) for new logging code.

#define PSPDFLogVerbose(format, ...)                                                   \
    PSPDFLogMacro(PSPDFLogLevelMaskVerbose, NULL, (^{                                  \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFLogDebug(format, ...)                                                     \
    PSPDFLogMacro(PSPDFLogLevelMaskDebug, NULL, (^{                                    \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFLog(format, ...)                                                          \
    PSPDFLogMacro(PSPDFLogLevelMaskInfo, NULL, (^{                                     \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFLogWarning(format, ...)                                                   \
    PSPDFLogMacro(PSPDFLogLevelMaskWarning, NULL, (^{                                  \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFLogError(format, ...)                                                     \
    PSPDFLogMacro(PSPDFLogLevelMaskError, NULL, (^{                                    \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFLogCritical(format, ...)                                                  \
    PSPDFLogMacro(PSPDFLogLevelMaskCritical, NULL, (^{                                 \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))

// Variants that tag a tag as c string next to the log message:
// Use when you need a one-off logger without wanting to define a category name.
// TLog is preferred.

#define PSPDFTLogVerbose(tag, format, ...)                                            \
    PSPDFLogMacro(PSPDFLogLevelMaskVerbose, tag, (^{                                  \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFTLog(tag, format, ...)                                                   \
    PSPDFLogMacro(PSPDFLogLevelMaskInfo, tag, (^{                                     \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFTLogWarning(ftag, ormat, ...)                                            \
    PSPDFLogMacro(PSPDFLogLevelMaskWarning, tag, (^{                                  \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFTLogError(tag, format, ...)                                              \
    PSPDFLogMacro(PSPDFLogLevelMaskError, tag, (^{                                    \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFTLogDebug(tag, format, ...)                                              \
    PSPDFLogMacro(PSPDFLogLevelMaskDebug, tag, (^{                                    \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define PSPDFTLogCritical(tag, format, ...)                                           \
    PSPDFLogMacro(PSPDFLogLevelMaskCritical, tag, (^{                                 \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))

// Variant that uses the 'LogTag' string defined in the file:
// Usage: Logger("MyComponentName");

#define Logger(tag) static let LogTag = tag

#define TLogVerbose(format, ...)                                                      \
    PSPDFLogMacro(PSPDFLogLevelMaskVerbose, LogTag, (^{                               \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define TLog(format, ...)                                                             \
    PSPDFLogMacro(PSPDFLogLevelMaskInfo, LogTag, (^{                                  \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define TLogWarning(format, ...)                                                      \
    PSPDFLogMacro(PSPDFLogLevelMaskWarning, LogTag, (^{                               \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define TLogError(format, ...)                                                        \
    PSPDFLogMacro(PSPDFLogLevelMaskError, LogTag, (^{                                 \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define TLogDebug(format, ...)                                                        \
    PSPDFLogMacro(PSPDFLogLevelMaskDebug, LogTag, (^{                                 \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))
#define TLogCritical(format, ...)                                                     \
    PSPDFLogMacro(PSPDFLogLevelMaskCritical, LogTag, (^{                              \
                      return [NSString stringWithFormat:(@"" format), ##__VA_ARGS__]; \
                  }))

#if defined(PSPDF_BLOCK_LOGGING)
#define PSPDFLogWarningOnce(format, ...)
#else
#define PSPDFLogWarningOnce(format, ...)            \
    do {                                            \
        static dispatch_once_t onceToken;           \
        dispatch_once(&onceToken, ^{                \
            PSPDFLogWarning(format, ##__VA_ARGS__); \
        });                                         \
    } while (0)
#endif
