//
//  Copyright © 2020-2021 PSPDFKit GmbH. All rights reserved.
//
//  THIS SOURCE CODE AND ANY ACCOMPANYING DOCUMENTATION ARE PROTECTED BY INTERNATIONAL COPYRIGHT LAW
//  AND MAY NOT BE RESOLD OR REDISTRIBUTED. USAGE IS BOUND TO THE PSPDFKIT LICENSE AGREEMENT.
//  UNAUTHORIZED REPRODUCTION OR DISTRIBUTION IS SUBJECT TO CIVIL AND CRIMINAL PENALTIES.
//  This notice may not be removed from this file.
//

#import <PSPDFKit/PSPDFDocument.h>
#import <PSPDFKit/PSPDFEnvironment.h>

NS_ASSUME_NONNULL_BEGIN

PSPDF_CLASS_SWIFT(OfficeDocument)
@interface PSPDFOfficeDocument : PSPDFDocument

/// Creates a new PDF document via Native PSPDFKit Office conversion.
///
/// @param officeURL A file URL of the docx file.
/// @return A new office document instance.
///
/// @note This method requires the Office Documents feature to be enabled for your license.
- (instancetype)initWithOfficeURL:(NSURL *)officeURL;

/// The office file URL, if the Office document was initialized via `initWithOfficeURL:` or with a file data provider.
@property (nullable, nonatomic, readonly) NSURL *officeURL;

/// The office file is loaded and converted to PDF asynchronously. Use this call to wait until the process is complete.
///
/// @note This will block the current thread.
- (void)waitUntilLoaded;

/// The data provider that the office document was initialized with.
@property (nullable, nonatomic, readonly) id<PSPDFDataProviding> officeDataProvider;

/// A set containing UTI types supported by the office document class.
@property (nonatomic, class, readonly) NSSet<NSString *> *supportedContentTypes;

@end

@interface PSPDFOfficeDocument (Unavailable)

PSPDF_EMPTY_INIT_UNAVAILABLE
- (instancetype)initWithURL:(NSURL *)url PSPDF_NOT_DESIGNATED_INITIALIZER_ATTRIBUTE;
- (instancetype)initWithDataProviders:(NSArray<id<PSPDFDataProviding>> *)dataProviders loadCheckpointIfAvailable:(BOOL)loadCheckpoint PSPDF_NOT_DESIGNATED_INITIALIZER_ATTRIBUTE;
- (instancetype)initWithDataProviders:(NSArray<id<PSPDFDataProviding>> *)dataProviders PSPDF_NOT_DESIGNATED_INITIALIZER_ATTRIBUTE;

@end

NS_ASSUME_NONNULL_END
