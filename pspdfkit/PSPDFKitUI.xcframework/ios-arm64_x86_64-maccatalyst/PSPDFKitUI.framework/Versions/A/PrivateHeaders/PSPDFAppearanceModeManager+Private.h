//
//  Copyright © 2016-2021 PSPDFKit GmbH. All rights reserved.
//
//  THIS SOURCE CODE AND ANY ACCOMPANYING DOCUMENTATION ARE PROTECTED BY INTERNATIONAL COPYRIGHT LAW
//  AND MAY NOT BE RESOLD OR REDISTRIBUTED. USAGE IS BOUND TO THE PSPDFKIT LICENSE AGREEMENT.
//  UNAUTHORIZED REPRODUCTION OR DISTRIBUTION IS SUBJECT TO CIVIL AND CRIMINAL PENALTIES.
//  This notice may not be removed from this file.
//

#import <PSPDFKitUI/PSPDFAppearanceModeManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface PSPDFAppearanceModeManager ()

/// @section Mode operations

/// Returns the document render options for the provided mode.
- (PSPDFRenderOptions *)renderOptionsForMode:(PSPDFAppearanceMode)mode withCurrentOptions:(PSPDFRenderOptions *)options;

/// Invokes the deprecated `appearanceManager:applyAppearanceSettingsForMode:` delegate. Should be removed when the delegate is removed.
- (void)applyAppearanceSettingsForMode:(PSPDFAppearanceMode)mode;

/// Invokes the deprecated `appearanceManager:updateConfiguration:forMode:` delegate. Should be removed when the delegate is removed.
- (void)updateConfiguration:(PSPDFConfigurationBuilder *)builder forMode:(PSPDFAppearanceMode)mode;

@end

NS_ASSUME_NONNULL_END
